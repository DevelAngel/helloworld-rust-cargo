extern crate bindgen;

use std::env;
use std::path::PathBuf;

// C library
fn generate_bindings_doubler(out_path: &PathBuf) {
    println!("cargo:rustc-link-lib=doubler");
    println!("cargo:rerun-if-changed=doubler-wrapper.h");

    let bindings = bindgen::Builder::default()
        .header("doubler-wrapper.h")
        .parse_callbacks(Box::new(bindgen::CargoCallbacks))
        .generate()
        .expect("Unable to generate bindings for doubler");

    bindings
        .write_to_file(out_path.join("doubler-bindings.rs"))
        .expect("Couldn't write bindings for doubler!");
}

// C++ library
fn generate_bindings_mymath(out_path: &PathBuf) {
    println!("cargo:rustc-link-lib=MyMath");
    println!("cargo:rerun-if-changed=MyMath-wrapper.hpp");

    let bindings = bindgen::Builder::default()
        .header("MyMath-wrapper.hpp")
        .parse_callbacks(Box::new(bindgen::CargoCallbacks))
        .generate()
        .expect("Unable to generate bindings for MyMath");

    bindings
        .write_to_file(out_path.join("MyMath-bindings.rs"))
        .expect("Couldn't write bindings for MyMath!");
}

fn main() {
    let out_path = PathBuf::from(env::var("OUT_DIR").unwrap());
    generate_bindings_doubler(&out_path);
    generate_bindings_mymath(&out_path);
}

