#![allow(non_upper_case_globals)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]

include!(concat!(env!("OUT_DIR"), "/doubler-bindings.rs"));
include!(concat!(env!("OUT_DIR"), "/MyMath-bindings.rs"));

fn main() {
    println!("Hello, world!");

    // call C library doubler
    unsafe {
        let n = 55;
        println!("doubler({}) = {}", n, doubler(n));
    }

    // call C++ library MyMath
    unsafe {
        let a = -12;
        let b = 34;
        println!("MyMath_add({a}, {b}) = {a} + {b} = {res}", a=a, b=b, res=MyMath_add(a, b));
        println!("MyMath_sub({a}, {b}) = {a} - {b} = {res}", a=a, b=b, res=MyMath_sub(a, b));
        println!("MyMath_mul({a}, {b}) = {a} * {b} = {res}", a=a, b=b, res=MyMath_mul(a, b));
    }
}
