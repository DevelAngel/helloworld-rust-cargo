# HelloWorld Rust-Cargo

This is a Hello World program written in Rust build with Cargo.
The program uses an external prebuild C/C++ library.

## Build

Please install `libclang` because rust-bindgen needs it to parse the header files
so that it is able to generate the corresponding rust source file `bindings.rs`.

Anyway, it is possible to link against the C/C++ code by using the GNU compiler collection.

### Prerequisite

This rust program calls one C library called doubler and one C++ library called MyMath:

 * doubler: https://gitlab.com/DevelAngel/helloworld-doubler-c-lib
 * MyMath: https://gitlab.com/DevelAngel/helloworld-mymath-cxx-lib

Compile both libraries and install them into the system or put them into a separate folder,
e.g. `$PWD/external/{lib,include}`.


### Debug

In the case that the libraries are installed into the system, use the following command
to build the program:

```sh
cargo build
```

If the libraries are installed somewhere else,
set some environment variables before building:

```sh
export LIBRARY_PATH="$PWD/external/lib"
export BINDGEN_EXTRA_CLANG_ARGS="-I$PWD/external/include"
cargo build
```

In this example, the shared libraries were installed into `$PWD/external/lib`
and their header files into `$PWD/external/include`.

### Release

To compile a release, simply add the option `--release` and use the environment variables
as in the Debug section to tell the compilation process where to find the C/C++ libraries.

```sh
cargo build --release
```

### Build error: Unable to find libclang

If the build command failes with the error `Unable to find libclang`
and you are sure that clang is installed, then the `libclang` shared library
was installed outside of the standard search folders.

```sh
   Compiling helloworld v0.1.0 (/home/develangel/Workspace/rust/helloworld-rust-cargo)
error: failed to run custom build command for `helloworld v0.1.0 (/home/develangel/Workspace/rust/helloworld-rust-cargo)`

Caused by:
  process didn't exit successfully: `/home/develangel/Workspace/rust/helloworld-rust-cargo/target/debug/build/helloworld-9c21a54a085bc469/build-script-build` (exit status: 101)
  --- stdout
  cargo:rustc-link-lib=doubler
  cargo:rustc-link-lib=MyMath
  cargo:rerun-if-changed=wrapper.h

  --- stderr
  thread 'main' panicked at 'Unable to find libclang: "couldn't find any valid shared libraries matching: ['libclang.so', 'libclang-*.so', 'libclang.so.*', 'libclang-*.so.*'], set the `LIBCLANG_PATH` environment variable to a path where one of these files can be found (invalid: [])"', /home/develangel/.cargo/registry/src/github.com-1ecc6299db9ec823/bindgen-0.53.3/src/lib.rs:1956:31
  note: run with `RUST_BACKTRACE=1` environment variable to display a backtrace
```

In Gentoo Linux, the package `sys-devel/clang:12` installs libclang into the
path `/usr/lib/llvm/12/lib64/`. Then, let the `LIBCLANG_PATH` environment variable
point to this path before executing the build command:

```sh
export LIBCLANG_PATH="/usr/lib/llvm/12/lib64/libclang.so"
cargo build
cargo build --release
```

## Execution

```sh
./target/debug/helloworld
./target/release/helloworld
```

If the shared libraries are not installed into the system,
let the `LD_LIBRARY_PATH` environment variable point to the shared C/C++ libraries
to be able to execute the `helloworld` program:

```sh
export LD_LIBRARY_PATH="$PWD/external/lib"
./target/debug/helloworld
./target/release/helloworld
```
